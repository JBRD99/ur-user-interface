def Wave():
    acc = 1
    vel = 0.5
    baseDeg = 1.57
    #sleepTime = 0.5
    movej([baseDeg,   -1.57,  0,  -1.57,  0,  0], a=acc, v=vel)
    #sleep(sleepTime)
    movej([baseDeg,   -1.57,  0.5,  -1.57,  0,  0], a=acc, v=vel)
    #sleep(sleepTime)
    movej([baseDeg,   -1.57,  -0.5,  -1.57,  0,  0], a=acc, v=vel)
    #sleep(sleepTime)
    movej([baseDeg,   -1.57,  0,  -1.57,  0,  0], a=acc, v=vel)
end