from ur_ui import Ui_MainWindow
from ur_receive import URReceive
from ur_sender import URSender
from tools import Tools
from PyQt5.QtWidgets import QMainWindow, QApplication, QMessageBox, QFileDialog
from sys import exit as sys_exit
from sys import argv as sys_argv
import socket
import json
import time

class URController(QMainWindow, Ui_MainWindow): 

    def __init__(self):
        ## Start and show UI
        self.tools = Tools(self)
        self.receive = URReceive(self)
        self.send = URSender(self)
        self.load_files()
        self.start_ui()

    def start_ui(self):
        app = QApplication(sys_argv)
        app.setStyle('Fusion') 
        QMainWindow.__init__(self)
        self.setupUi(self)
        self.setupSignals()  
        self.show()              
        sys_exit(app.exec_())

    def load_files(self):
        f = open("lib/ur_messages.json")
        self.urMsgs = json.load(f)
        f.close()
        f = open("lib/value_types.json")
        valueTypes = json.load(f)
        f.close()
        self.valueTypes = valueTypes["valueTypes"]

    def load_ur_script_file(self):
        try:
            fname = QFileDialog.getOpenFileName(self, 'Open file', '.\\urscripts',"Text files (*.txt)")
            f = open(fname[0], 'r')
            filename = fname[0].split('/')[-1]
            with f:
                data = f.read()
                self.textEdit_ur_script.setText(data)
                self.lineEdit_file_name.setText(filename)
                f.close()
            self.tools.write_to_console_output("Read content from file (" + filename + ")")
        except Exception as e:
            self.tools.write_to_console_output("Error occured",e)

    def save_ur_script_file(self):
        try:
            f = open(".\\urscripts\\" + self.lineEdit_file_name.text(), 'w')
            with f:
                f.write(self.textEdit_ur_script.toPlainText())
                f.close()
            self.tools.write_to_console_output("Saved content to file (" + self.lineEdit_file_name.text() + ")")
        except Exception as e:
            self.tools.write_to_console_output("Error occured",e)


    def connect_to_robot(self):
        try:
            self.tools.write_to_console_output("Attempting to connect to UR Robot")
            self.client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.client.settimeout(2)
            self.client.connect((socket.gethostbyname(self.txtbox_ip_address.text()),30001))
            self.tools.write_to_console_output("Connected to UR Robot")
            for i in range(2):
                self.receive.receiveData()
            self.elementsEnabled(True)
        except:
            self.tools.write_to_console_output("Failed to connect to UR Robot")
            self.elementsEnabled(False)
            popupBox = QMessageBox()
            popupBox.setWindowTitle("Failed to connect to UR Robot")
            popupBox.setIcon(QMessageBox.Critical)
            popupBox.setText("Error while connecting to the robot.\nPossible solutions: \n-Check if the robot is powered on.\n-Ensure that the robot has proper network configuration.")
            popupBox.setStandardButtons(QMessageBox.Ok | QMessageBox.Retry)
            if popupBox.exec_() == QMessageBox.Retry:
                self.connect_to_robot()

    def elementsEnabled(self, isEnabled):
        self.checkbox_isConnected.setChecked(isEnabled)
        self.btn_receiveData.setEnabled(isEnabled)

        self.btn_run_ur_script.setEnabled(isEnabled)

        self.move_hand_bwd_btn.setEnabled(isEnabled)
        self.move_hand_fwd_btn.setEnabled(isEnabled)
        self.move_hand_left_btn.setEnabled(isEnabled)
        self.move_hand_right_btn.setEnabled(isEnabled)
        self.move_hand_down_btn.setEnabled(isEnabled)
        self.move_hand_up_btn.setEnabled(isEnabled)

        self.hand_controls_angle_rollminus.setEnabled(isEnabled)
        self.hand_controls_angle_rollplus.setEnabled(isEnabled)
        self.hand_controls_angle_yawminus.setEnabled(isEnabled)
        self.hand_controls_angle_yawplus.setEnabled(isEnabled)
        self.hand_controls_angle_pitchminus.setEnabled(isEnabled)
        self.hand_controls_angle_pitchplus.setEnabled(isEnabled)

        self.toolcontrol_x.setEnabled(isEnabled)
        self.toolcontrol_y.setEnabled(isEnabled)
        self.toolcontrol_z.setEnabled(isEnabled)
        self.toolcontrol_rx.setEnabled(isEnabled)
        self.toolcontrol_ry.setEnabled(isEnabled)
        self.toolcontrol_rz.setEnabled(isEnabled)
        self.toolcontrol_mtp.setEnabled(isEnabled)
        self.toolcontrol_home.setEnabled(isEnabled)

        self.jointctrl_pos_base.setEnabled(isEnabled)
        self.jointctrl_pos_shoulder.setEnabled(isEnabled)
        self.jointctrl_pos_elbow.setEnabled(isEnabled)
        self.jointctrl_pos_wrist_1.setEnabled(isEnabled)
        self.jointctrl_pos_wrist_2.setEnabled(isEnabled)
        self.jointctrl_pos_wrist_3.setEnabled(isEnabled)
    
    def setupSignals(self):
        self.btn_connect.clicked.connect(lambda: self.connect_to_robot())
        self.btn_receiveData.clicked.connect(lambda: self.receive.receiveData(True))
        self.btn_load_file.clicked.connect(lambda: self.load_ur_script_file())
        self.btn_save_file.clicked.connect(lambda: self.save_ur_script_file())
        self.move_hand_fwd_btn.clicked.connect(lambda: self.send.move_position("fwd"))
        self.move_hand_bwd_btn.clicked.connect(lambda: self.send.move_position("bwd"))
        self.move_hand_up_btn.clicked.connect(lambda: self.send.move_position("up"))
        self.move_hand_down_btn.clicked.connect(lambda: self.send.move_position("down"))
        self.move_hand_left_btn.clicked.connect(lambda: self.send.move_position("left"))
        self.move_hand_right_btn.clicked.connect(lambda: self.send.move_position("right"))
        self.btn_run_ur_script.clicked.connect(lambda: self.send.runURScript())
        self.toolcontrol_home.clicked.connect(lambda: self.send.homePos())
        self.jointctrl_pos_base.sliderReleased.connect(lambda: self.send.moveJ([
                                                                        self.jointctrl_pos_base.value(), 
                                                                        self.jointctrl_pos_shoulder.value(), 
                                                                        self.jointctrl_pos_elbow.value(), 
                                                                        self.jointctrl_pos_wrist_1.value(), 
                                                                        self.jointctrl_pos_wrist_2.value(), 
                                                                        self.jointctrl_pos_wrist_3.value()
                                                                        ]))
        self.jointctrl_pos_shoulder.sliderReleased.connect(lambda: self.send.moveJ([
                                                                        self.jointctrl_pos_base.value(), 
                                                                        self.jointctrl_pos_shoulder.value(), 
                                                                        self.jointctrl_pos_elbow.value(), 
                                                                        self.jointctrl_pos_wrist_1.value(), 
                                                                        self.jointctrl_pos_wrist_2.value(), 
                                                                        self.jointctrl_pos_wrist_3.value()
                                                                        ]))
        self.jointctrl_pos_elbow.sliderReleased.connect(lambda: self.send.moveJ([
                                                                        self.jointctrl_pos_base.value(), 
                                                                        self.jointctrl_pos_shoulder.value(), 
                                                                        self.jointctrl_pos_elbow.value(), 
                                                                        self.jointctrl_pos_wrist_1.value(), 
                                                                        self.jointctrl_pos_wrist_2.value(), 
                                                                        self.jointctrl_pos_wrist_3.value()
                                                                        ]))   
        self.jointctrl_pos_wrist_1.sliderReleased.connect(lambda: self.send.moveJ([
                                                                        self.jointctrl_pos_base.value(), 
                                                                        self.jointctrl_pos_shoulder.value(), 
                                                                        self.jointctrl_pos_elbow.value(), 
                                                                        self.jointctrl_pos_wrist_1.value(), 
                                                                        self.jointctrl_pos_wrist_2.value(), 
                                                                        self.jointctrl_pos_wrist_3.value()
                                                                        ]))
        self.jointctrl_pos_wrist_2.sliderReleased.connect(lambda: self.send.moveJ([
                                                                        self.jointctrl_pos_base.value(), 
                                                                        self.jointctrl_pos_shoulder.value(), 
                                                                        self.jointctrl_pos_elbow.value(), 
                                                                        self.jointctrl_pos_wrist_1.value(), 
                                                                        self.jointctrl_pos_wrist_2.value(), 
                                                                        self.jointctrl_pos_wrist_3.value()
                                                                        ]))
        self.jointctrl_pos_wrist_3.sliderReleased.connect(lambda: self.send.moveJ([
                                                                        self.jointctrl_pos_base.value(), 
                                                                        self.jointctrl_pos_shoulder.value(), 
                                                                        self.jointctrl_pos_elbow.value(), 
                                                                        self.jointctrl_pos_wrist_1.value(), 
                                                                        self.jointctrl_pos_wrist_2.value(), 
                                                                        self.jointctrl_pos_wrist_3.value()
                                                                        ]))
        self.toolcontrol_mtp.clicked.connect(lambda: self.send.moveJ_p([
                                                                        self.toolcontrol_x.value()/1000, 
                                                                        self.toolcontrol_y.value()/1000,
                                                                        self.toolcontrol_z.value()/1000,
                                                                        self.toolcontrol_rx.value(),
                                                                        self.toolcontrol_ry.value(),
                                                                        self.toolcontrol_rz.value()
                                                                        ]))
        self.btn_clearLog.clicked.connect(lambda: self.list_consoleLog.clear())

if __name__ == "__main__":

    controller = URController()