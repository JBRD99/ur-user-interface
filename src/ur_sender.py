import sys

class URSender():

    def __init__(self, contr):
        self.contr = contr
        self.a = 1 ## Acceleration
        self.v = 0.5 ## Velocity

    def sendCommand(self, funcName, cmd):
        cmd = f"def customGUI_{funcName}():\n{cmd}\nend\n"
        print(cmd)
        self.contr.client.sendall(cmd.encode())
        self.contr.receive.receiveData()
        self.contr.tools.write_to_console_output(f"Sent command to UR Robot")
        
    def runURScript(self):
        cmd = self.contr.textEdit_ur_script.toPlainText() + "\n"
        self.contr.client.sendall(cmd.encode())
        self.contr.tools.write_to_console_output("Sent URScript to UR Robot")

    def moveJ_p(self, toolPos):
        moveCommand = f"movej(p{toolPos}, a={self.a}, v={self.v})"
        self.sendCommand(sys._getframe().f_code.co_name, moveCommand)

    def moveJ(self, jointPos):
        self.contr.tools.update_joint_pos_ui_values(jointPos, True)
        jointPos = self.contr.tools.convert_degrees_to_radian(jointPos)
        moveCommand = f"movej({jointPos}, a={self.a}, v={self.v})"
        self.sendCommand(sys._getframe().f_code.co_name, moveCommand)

    def homePos(self):
        jointPos = [0, -1.57, 0, -1.57, 0, 0]
        self.contr.toolcontrol_x.setValue(-0.81)
        self.contr.toolcontrol_y.setValue(-232.9)
        self.contr.toolcontrol_z.setValue(1079.4)
        self.contr.toolcontrol_rx.setValue(0.002)
        self.contr.toolcontrol_ry.setValue(2.221)
        self.contr.toolcontrol_rz.setValue(-2.221)
        self.contr.tools.update_joint_pos_ui_values(jointPos, False)
        homePos = f"movej({jointPos}, a={self.a}, v={self.v})"
        self.sendCommand(sys._getframe().f_code.co_name, homePos)        

    def move_position(self, direction):
        increment_val = 20
        match direction:
            case "right":
                cartesianData = [self.contr.toolcontrol_x.value()/1000,(self.contr.toolcontrol_y.value()+increment_val)/1000,self.contr.toolcontrol_z.value()/1000,self.contr.toolcontrol_rx.value(),self.contr.toolcontrol_ry.value(),self.contr.toolcontrol_rz.value()]
                moveCommand = f"movej(p{cartesianData}, a={self.a}, v={self.v})"
            case "left":
                cartesianData = [self.contr.toolcontrol_x.value()/1000,(self.contr.toolcontrol_y.value()-increment_val)/1000,self.contr.toolcontrol_z.value()/1000,self.contr.toolcontrol_rx.value(),self.contr.toolcontrol_ry.value(),self.contr.toolcontrol_rz.value()]
                moveCommand = f"movej(p{cartesianData}, a={self.a}, v={self.v})"
            case "up":
                cartesianData = [self.contr.toolcontrol_x.value()/1000,self.contr.toolcontrol_y.value()/1000,(self.contr.toolcontrol_z.value()+increment_val)/1000,self.contr.toolcontrol_rx.value(),self.contr.toolcontrol_ry.value(),self.contr.toolcontrol_rz.value()]
                moveCommand = f"movej(p{cartesianData}, a={self.a}, v={self.v})"
            case "down":
                cartesianData = [self.contr.toolcontrol_x.value()/1000,self.contr.toolcontrol_y.value()/1000,(self.contr.toolcontrol_z.value()-increment_val)/1000,self.contr.toolcontrol_rx.value(),self.contr.toolcontrol_ry.value(),self.contr.toolcontrol_rz.value()]
                moveCommand = f"movej(p{cartesianData}, a={self.a}, v={self.v})"
            case "fwd":
                cartesianData = [(self.contr.toolcontrol_x.value()-increment_val)/1000,self.contr.toolcontrol_y.value()/1000,self.contr.toolcontrol_z.value()/1000,self.contr.toolcontrol_rx.value(),self.contr.toolcontrol_ry.value(),self.contr.toolcontrol_rz.value()]
                moveCommand = f"movej(p{cartesianData}, a={self.a}, v={self.v})"
            case "bwd":
                cartesianData = [(self.contr.toolcontrol_x.value()+increment_val)/1000,self.contr.toolcontrol_y.value()/1000,self.contr.toolcontrol_z.value()/1000,self.contr.toolcontrol_rx.value(),self.contr.toolcontrol_ry.value(),self.contr.toolcontrol_rz.value()]
                moveCommand = f"movej(p{cartesianData}, a={self.a}, v={self.v})"
        self.contr.toolcontrol_x.setValue(cartesianData[0]*1000)
        self.contr.toolcontrol_y.setValue(cartesianData[1]*1000)
        self.contr.toolcontrol_z.setValue(cartesianData[2]*1000)
        self.contr.toolcontrol_rx.setValue(cartesianData[3])
        self.contr.toolcontrol_ry.setValue(cartesianData[4])
        self.contr.toolcontrol_rz.setValue(cartesianData[5])
        self.sendCommand(sys._getframe().f_code.co_name, moveCommand)
    
