import math

class SubPackageHandler():

    def __init__(self) -> None:
        self.data = []

    def jointData(self, jointData):
        for key, val in jointData.items():
            if key[-9:] != "jointMode":
                self.data.append(f"{key} : {round(val*180/math.pi,2)}")
            else: self.data.append(f"{key} : {val}")
        return self.data


    def cartesianInfo(self, cartesianInfo):
        for key, val in cartesianInfo.items():
            if key[0:1] != "R":
                self.data.append(f"{key} : {round(val*1000,2)}")
            else:
                self.data.append(f"{key} : {round(val,3)}")
        return self.data