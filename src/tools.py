from math import radians, degrees

class Tools():

    def __init__(self, contr):
        self.contr = contr

    ## ---------------------------------------------------------------------------------------------------
    ## Writes to UI console output and prints message to terminal
    ## ---------------------------------------------------------------------------------------------------  
    def write_to_console_output(self, msg):
        self.contr.list_consoleLog.addItem(msg)
        self.contr.list_consoleLog.scrollToBottom()

    def convert_degrees_to_radian(self, degrees=[]):
        data = []
        for degree in degrees:
            data.append(round(radians(degree),3))
        return data

    def update_ui_value(self, value):
        ui_element = value.split()[0]
        value = float(value.split()[2])
        if ui_element[-6:] == "actual":
            value = int(value)
        match ui_element:
            case "base_q_actual":
                self.contr.readonly_jointctrl_pos_base.setValue(value)
                self.contr.jointctrl_pos_base.setValue(value)
            case "shoulder_q_actual":
                self.contr.readonly_jointctrl_pos_shoulder.setValue(value)
                self.contr.jointctrl_pos_shoulder.setValue(value)
            case "elbow_q_actual":
                self.contr.readonly_jointctrl_pos_elbow.setValue(value)
                self.contr.jointctrl_pos_elbow.setValue(value)
            case "wrist1_q_actual":
                self.contr.readonly_jointctrl_pos_wrist_1.setValue(value)
                self.contr.jointctrl_pos_wrist_1.setValue(value)
            case "wrist2_q_actual":
                self.contr.readonly_jointctrl_pos_wrist_2.setValue(value)
                self.contr.jointctrl_pos_wrist_2.setValue(value)
            case "wrist3_q_actual":
                self.contr.readonly_jointctrl_pos_wrist_3.setValue(value)
                self.contr.jointctrl_pos_wrist_3.setValue(value)
            case "X":
                self.contr.toolcontrol_x.setValue(value)
            case "Y":
                self.contr.toolcontrol_y.setValue(value)
            case "Z":
                self.contr.toolcontrol_z.setValue(value)
            case "Rx":
                self.contr.toolcontrol_rx.setValue(value)
            case "Ry":
                self.contr.toolcontrol_ry.setValue(value)
            case "Rz":
                self.contr.toolcontrol_rz.setValue(value)

    def update_joint_pos_ui_values(self, values, isDegree):
        update_joint_ui = {}
        for idx, val in enumerate(values):
            if isDegree:
                update_joint_ui[idx] = int(val)
            else:
                update_joint_ui[idx] = int(degrees(val))
        for key, val in update_joint_ui.items():
            match key:
                case 0:
                    self.contr.readonly_jointctrl_pos_base.setValue(val)
                    self.contr.jointctrl_pos_base.setValue(val)
                case 1:
                    self.contr.readonly_jointctrl_pos_shoulder.setValue(val)
                    self.contr.jointctrl_pos_shoulder.setValue(val)
                case 2:
                    self.contr.readonly_jointctrl_pos_elbow.setValue(val)
                    self.contr.jointctrl_pos_elbow.setValue(val)
                case 3:
                    self.contr.readonly_jointctrl_pos_wrist_1.setValue(val)
                    self.contr.jointctrl_pos_wrist_1.setValue(val)
                case 4:
                    self.contr.readonly_jointctrl_pos_wrist_2.setValue(val)
                    self.contr.jointctrl_pos_wrist_2.setValue(val)
                case 5:
                    self.contr.readonly_jointctrl_pos_wrist_3.setValue(val)
                    self.contr.jointctrl_pos_wrist_3.setValue(val)