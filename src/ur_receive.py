import socket
from struct import *
from time import sleep
from sys import stdout
import json
from ur_subPkgHandler import SubPackageHandler


class URReceive():

    def __init__(self, contr):
        self.contr = contr
        self.subPkgHandler = SubPackageHandler()
        self.filterSubPkgs = [5, 10]

    def unpackHelper(self, sIdx, eIdx, val_type):
        eIdx, val_type = sIdx + self.contr.valueTypes[val_type][1], self.contr.valueTypes[val_type][0]
        return sIdx, eIdx, val_type

    def decodeSubPkg(self, name, variables, isRawData, sIdx, eIdx):
        subPkgData = {}
        startIndex, endIndex = sIdx, eIdx 
        for var_key, var_val in variables.items():
            startIndex, endIndex, valueType = self.unpackHelper(endIndex, startIndex, var_val)
            #print(f"Start index: {startIndex}, End index: {endIndex}")
            subPkgData[var_key] = unpack(f'!{valueType}', self.data[startIndex:endIndex])[0]
        if isRawData:
            for func in filter(lambda x: not(str(x).startswith('__')), dir(self.subPkgHandler)):
                if func == name:
                    for msg in getattr(self.subPkgHandler, name)(subPkgData):
                        self.contr.tools.write_to_console_output(msg)
                        self.contr.tools.update_ui_value(msg)
        else:
            for var_key, var_val in subPkgData.items(): 
                self.contr.tools.write_to_console_output(f"{var_key} : {var_val}")
        return startIndex, endIndex

    def receiveData(self, prompt=False):
        if self.contr.list_consoleLog.count() > 1000:
            self.contr.list_consoleLog.clear()
        receivedInfo = False
        while receivedInfo == False:
            idx = 0
            self.data = self.contr.client.recv(2048)
            startIndex, endIndex, valueType = self.unpackHelper(idx,idx,"int")
            msgLen = unpack(f'!{valueType}', self.data[startIndex:endIndex])[0]
            startIndex, endIndex, valueType = self.unpackHelper(endIndex,startIndex,"unsigned char")
            messageType = unpack(f'!{valueType}', self.data[startIndex:endIndex])[0]
            if prompt == False:
                self.contr.tools.write_to_console_output(f"< Length of data: {len(self.data)} >")
                self.contr.tools.write_to_console_output("Package length: " + str(msgLen))
                self.contr.tools.write_to_console_output("Message type: " + str(messageType))
            for msg in self.contr.urMsgs["messages"]:
                if messageType == msg["messageType"]:
                    receivedInfo = True
                    self.contr.tools.write_to_console_output(f"<< Received message type: {msg['name']}>>")
                    subPkgLenCount = 0
                    while subPkgLenCount < msgLen:
                        if messageType == 20:
                            startIndex, endIndex, valueType = self.unpackHelper(endIndex,startIndex,"unsigned long long")
                            timestamp = unpack(f'!{valueType}', self.data[startIndex:endIndex])[0]
                            self.contr.tools.write_to_console_output("Timestamp: " + str(timestamp))
                            startIndex, endIndex, valueType = self.unpackHelper(endIndex,startIndex,"signed char")
                            source = unpack(f'!{valueType}', self.data[startIndex:endIndex])[0]
                            self.contr.tools.write_to_console_output("Source: " + str(source))
                            startIndex, endIndex, valueType = self.unpackHelper(endIndex,startIndex,"signed char")
                            subPkgType = unpack(f'!{valueType}', self.data[startIndex:endIndex])[0]
                            subPkgLen = msgLen
                        else:
                            startIndex, endIndex, valueType = self.unpackHelper(endIndex, startIndex ,"unsigned int")
                            subPkgLen = unpack(f'!{valueType}', self.data[startIndex:endIndex])[0]
                            startIndex, endIndex, valueType = self.unpackHelper(endIndex,startIndex,"unsigned char")
                            subPkgType = unpack(f'!{valueType}', self.data[startIndex:endIndex])[0]
                            self.contr.tools.write_to_console_output("Sub package length: " + str(subPkgLen))
                            if subPkgType in self.filterSubPkgs:
                                break
                        self.contr.tools.write_to_console_output("Sub package type: " + str(subPkgType))
                        for subPkg in msg["subPackages"]:
                            if subPkgType == subPkg["packageType"]:
                                self.contr.tools.write_to_console_output(f"<<< Received subpackage type: {subPkg['name']}>>>")
                                startIndex, endIndex = self.decodeSubPkg(subPkg["name"], subPkg["variables"] ,subPkg["rawData"] ,startIndex, endIndex)
                        
                        subPkgLenCount += subPkgLen
                    self.contr.tools.write_to_console_output("<<Received information>>")
            if prompt == False:
                break
                